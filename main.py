from moviepy.editor import *
import multiprocessing
import cv2
import concurrent
import concurrent.futures
import numpy
import time
import argparse

slices = 0

arg_parser = argparse.ArgumentParser()
arg_parser.add_argument('--video_path', help="Path of video", default="videos/armas.mp4")
args = arg_parser.parse_args()


def get_video_duration(video_clip_path):
    duration = VideoFileClip(video_clip_path).duration
    print(f"Duration: {duration}")
    return duration


"""
def split_video(video_clip_path, init_time, end_time, portion):
    try:
        result = VideoFileClip(video_clip_path).subclip(init_time, end_time)
        result.write_videofile(f"spliced_videos/{portion}.mp4")
    except multiprocessing:
        print("error", init_time, end_time)
    finally:
        pool_sema.release()
"""


def split_video(video_clip_path, init_time, end_time, portion):
    result = VideoFileClip(video_clip_path).subclip(init_time, end_time)
    result.write_videofile(f"spliced_videos/{portion}.mp4", threads=multiprocessing.cpu_count(), audio=False)
    result.close()


def split_video_multiprocessing(video_clip_path):
    slices = multiprocessing.cpu_count()
    slides_portion = get_video_duration(video_clip_path) / slices
    print(f"Portions: {slides_portion}")
    print(f"slices: {slices}")
    with concurrent.futures.ThreadPoolExecutor() as executor:
        for x in range(0, slices):
            print(f"Init time: {x * slides_portion} End time: {(x + 1) * slides_portion} Name: {x}")
            result = executor.submit(split_video, video_clip_path, x * slides_portion, (x + 1) * slides_portion, x)


def split_video_threads(video_clip_path):
    slices = multiprocessing.cpu_count()
    slides_portion = get_video_duration(video_clip_path) / slices
    for x in range(0, slices):
        split_video(video_clip_path, x * slides_portion, (x + 1) * slides_portion, x)


def get_frame(sec, video_capture, thread, number):
    video_capture.set(cv2.CAP_PROP_POS_MSEC, sec * 1000)
    has_frame, image = video_capture.read()
    if has_frame:
        cv2.imwrite(f"images/{thread}{str(number)}.jpg", image)
    return has_frame


def video_to_images(video_clip_path, thread):
    video_capture = cv2.VideoCapture(video_clip_path)
    sec = 0
    frame_rate = 1
    count = 0
    success = get_frame(sec, video_capture, thread, count)
    while success:
        count += 1
        sec = sec + frame_rate
        sec = round(sec, 2)
        success = get_frame(sec, video_capture, thread, count)
    return success


def splice_video():
    with concurrent.futures.ThreadPoolExecutor() as executor:
        for filename in os.listdir("spliced_videos"):
            executor.submit(video_to_images, f"spliced_videos/{filename}", f"THREAD{filename}-")


def load_yolo():
    net = cv2.dnn.readNet("yolov3.weights", "yolov3.cfg")
    classes = []
    with open("obj.names", "r") as f:
        classes = [line.strip() for line in f.readlines()]

    layers_names = net.getLayerNames()
    output_layers = [layers_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]
    colors = numpy.random.uniform(0, 255, size=(len(classes), 3))
    return net, classes, colors, output_layers


def load_image(img_path):
    img = cv2.imread(img_path)
    img = cv2.resize(img, None, fx=0.4, fy=0.4)
    height, width, channels = img.shape
    return img, height, width, channels


def detect_objects(img, net, output_layers):
    blob = cv2.dnn.blobFromImage(img, scalefactor=0.00392, size=(320, 320), mean=(0, 0, 0), swapRB=True, crop=False)
    net.setInput(blob)
    outputs = net.forward(output_layers)
    return blob, outputs


def get_box_dimensions(outputs, height, width):
    boxes = []
    configurations = []
    class_ids = []
    for output in outputs:
        for detect in output:
            scores = detect[5:]
            class_id = numpy.argmax(scores)
            conf = scores[class_id]
            if conf > 0.3:
                center_x = int(detect[0] * width)
                center_y = int(detect[1] * height)
                w = int(detect[2] * width)
                h = int(detect[3] * height)
                x = int(center_x - w / 2)
                y = int(center_y - h / 2)
                boxes.append([x, y, w, h])
                configurations.append(float(conf))
                class_ids.append(class_id)
    return boxes, configurations, class_ids


def draw_object_labels(boxes, configurations, colors, class_ids, classes, img, img_name, number):
    founded = False
    indexes = cv2.dnn.NMSBoxes(boxes, configurations, 0.5, 0.4)
    font = cv2.FONT_HERSHEY_PLAIN
    for i in range(len(boxes)):
        if i in indexes:
            founded = True
            x, y, w, h = boxes[i]
            label = str(classes[class_ids[i]])

            color = 255
            if i < len(colors):
                color = colors[i]

            cv2.rectangle(img, (x, y), (x + w, y + h), color, 2)
            cv2.putText(img, label, (x, y - 5), font, 1, color, 1)
    img = cv2.resize(img, (800, 600))
    if founded:
        cv2.imwrite(f"images_processed/{img_name}_{number}.jpg", img)


def image_detection(img_path, number):
    model, classes, colors, output_layers = load_yolo()
    image, height, width, channels = load_image(img_path)
    blob, outputs = detect_objects(image, model, output_layers)
    boxes, configurations, class_ids = get_box_dimensions(outputs, height, width)
    draw_object_labels(boxes, configurations, colors, class_ids, classes, image, img_path.split("/")[1], number)


def detection_by_thread(range_init, range_final):
    print(f"Init: {range_init} : End: {range_final}")
    counter = 0
    for filename in os.listdir("images"):
        if range_init <= counter <= range_final:
            image_detection(f"images/{filename}", counter)
        counter += 1


def run_yolo_detection():
    images_quantity = 0

    for filename in os.listdir("images"):
        images_quantity += 1

    images_by_thread = images_quantity / multiprocessing.cpu_count()
    images_by_thread_rounded = round(images_by_thread, 0)

    with concurrent.futures.ThreadPoolExecutor() as executor:
        for x in range(0, multiprocessing.cpu_count()):
            if x * images_by_thread_rounded + images_by_thread_rounded >= images_quantity:
                executor.submit(detection_by_thread, x * images_by_thread_rounded, images_quantity)
            else:
                executor.submit(detection_by_thread, x * images_by_thread_rounded,
                                x * images_by_thread_rounded + images_by_thread_rounded)


def clean(dir):
    for f in os.listdir(dir):
        os.remove(os.path.join(dir, f))


if __name__ == '__main__':
    video_path = args.video_path
    print("Video path: " + video_path)
    init_time = time.time()
    split_video_multiprocessing(video_path)
    splice_video()
    run_yolo_detection()
    print(f"The time of processing is : {time.time() - init_time} seconds")
    clean("spliced_videos")
    clean("images")

